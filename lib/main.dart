import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Score Counter',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Score Counter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int _counterLeft = 0;
  int _counterRight = 0;

  void _showResetDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text('Reset Counter'),
          content: Text('Are you sure want to reset?'),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text(
                'Reset',
                style: TextStyle(color: Colors.red)
              ),
              onPressed: () {
                _resetCounter();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _resetCounter() {
    setState(() {
      _counterLeft = 0;
      _counterRight = 0;
    });
  }

  void _incrementCounterLeft() {
    setState(() {
      _counterLeft++;
    });
  }
  void _decrementCounterLeft() {
    setState(() {
      if (_counterLeft > 0)
      _counterLeft--;
    });
  }

  void _incrementCounterRight() {
    setState(() {
      _counterRight++;
    });
  }
  void _decrementCounterRight() {
    setState(() {
      if (_counterRight > 0)
        _counterRight--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            // Kiri
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: _incrementCounterLeft,
                  child: Icon(Icons.add),
                ),
                Text(
                  '$_counterLeft',
                  style: TextStyle(
                    fontSize: 120.0
                  ),
                ),
                new RaisedButton(
                  onPressed: _decrementCounterLeft,
                  textColor: Colors.white,
                  color: Colors.red,
                  child: Icon(Icons.remove),
                )
              ],
            ),

            // Kanan
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: _incrementCounterRight,
                  child: Icon(Icons.add),
                ),
                Text(
                  '$_counterRight',
                  style: TextStyle(
                    fontSize: 120.0
                  )
                ),
                RaisedButton(
                  onPressed: _decrementCounterRight,
                  textColor: Colors.white,
                  color: Colors.red,
                  child: Icon(Icons.remove),
                )
              ],
            )

          ],
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showResetDialog,
        tooltip: 'Reset Counter',
        child: Icon(Icons.refresh),
      )
    );
  }
}